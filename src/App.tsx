import CarList from "./components/CarList";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AddNewCarPage from "./components/AddNewCarPage";
import { CarRepository } from "./services/CarRepository";
const carRepository = new CarRepository("http://localhost:8080/cars");

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<CarList repository={carRepository} />} />
        <Route
          path="/addnewcarpage"
          element={<AddNewCarPage repository={carRepository}  />}
        />
        <Route
          path="/addnewcarpage/:id"
          element={
            <AddNewCarPage
              repository={carRepository}
           
            />
          }
        />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
