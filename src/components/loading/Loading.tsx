import "./Loading.css"
import loading from "../../images/loading.gif"


const Loading = () => {
     return <div>
          <img className="loading" src={loading} />
     </div>

}

export default Loading