import Loading from "./loading/Loading";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { BASE_URL } from "../config";
import {
  DeleteAllCarsButton,
  EditCarButton,
  AddNewCarButton,
  DeleteCarButton,
  ReloadAllCarsButton,
} from "./buttons/Buttons";
import { CarRepository } from "../services/CarRepository";
import { CarListDto, CarEditDto } from "../services/CarTypes";
import   "./CarList.css"

interface CarListProps {
  repository: CarRepository;
}
type CarListState = {
  isLoading: boolean;
  cars: CarEditDto[];
};

const CarList = (props: CarListProps) => {
  const navigate = useNavigate();

  const [carsState, setCarsState] = useState<CarListState>({
    isLoading: true,
    cars: [],
  });

  async function loadCars() {
    setCarsState({
      isLoading: true,
      cars: [],
    });

    const data = await props.repository.getAll();

    setCarsState({
      isLoading: false,
      cars: data.cars,
    });
  }

  async function onDeleteCarClick(id: number) {
    setCarsState({
      ...carsState,
      isLoading: true,
    });

    await props.repository.deleteById(id);

    setCarsState({
      isLoading: false,
      cars: carsState.cars.filter((oneCar) => {
        return oneCar.id !== id;
      }),
    });
  }

  async function onDeleteAllClick() {
    setCarsState({
      ...carsState,
      isLoading: true,
    });

    await props.repository.deleteAll();
    setCarsState({
      isLoading: false,
      cars: [],
    });
  }

  async function onReloadAllClick() {
    loadCars();
  }

  async function onEditCarClick(id: number) {
    navigate(`/AddNewCarPage/${id}`);
  }

  async function onNewCarClick() {
    navigate("/AddNewCarPage");
  }

  useEffect(() => {
    loadCars();
  }, []);

  return (
    <section className="section-container">
      <h1 className="title">Vyber auto</h1>
      <div className="all-cars">
        {carsState.cars.map((oneCar) => {
          const { id, brand, age, description, price } = oneCar;
          const imgUrl = `${BASE_URL}/cars/${id}/photo?time=${Date.now()}`;
          return (
            <div key={id} className="one-car">
              <img src={imgUrl} alt={`Car ${id}`} />
              <div className="car-info">
                <h2>Značka : {brand}</h2>
                <h3>Rok výroby : {age} </h3>
                <p>Opis : {description}</p>
                <p className="car-price">Cena : {price} €</p>
              </div>
              <div className="car-actions">
                <DeleteCarButton onClick={() => onDeleteCarClick(id)} />
                <EditCarButton onClick={() => onEditCarClick(id)} />
              </div>
            </div>
          );
        })}
      </div>
      <div className="action-buttons">
        <DeleteAllCarsButton onClick={onDeleteAllClick} />
        <ReloadAllCarsButton onClick={onReloadAllClick} />
        <AddNewCarButton onClick={onNewCarClick} />
      </div>
      <div>{carsState.isLoading ? <Loading /> : ""}</div>
    </section>
  );
  
};

export default CarList;
