export type Car = {
    brand: string;
    age: number;
    description: string;
    price: number;
    img: string;
  };
  