import { useState } from "react";
import { ArrowBigLeft, ArrowBigRight } from "lucide-react";
import "./ImageSlider.css";

type ImageSliderProps = {
  imageUrls: string[];
};

export function ImageSlider({ imageUrls }: ImageSliderProps) {
  const [imageIndex, setImageIndex] = useState(0);

  function showPrevImg() {
    setImageIndex((index) => {
      if (index === 0) return imageUrls.length - 1;
      return index - 1;
    });
    console.log('showprev');
  }

  function showNextImg() {
    setImageIndex((index) => {
        if (index === imageUrls.length-1) return  0
        return index +1;
      });
      console.log('showNextImg');
  }

  return (
    <div className="slider">
      <img src={imageUrls[imageIndex]} className="img-slider-img" />
      <button
        onClick={showPrevImg}
        className="img-slider-btn"
        style={{ left: 0 }}
      >
        <ArrowBigLeft />
      </button>
      <button
        onClick={showNextImg}
        className="img-slider-btn"
        style={{ right: 0 }}
      >
        <ArrowBigRight />
      </button>
    </div>
  );
}
