import { useEffect, useState, FormEvent } from "react";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import { CarRepository } from "../services/CarRepository";
import { CarListDto, CarEditDto } from "../services/CarTypes";
import { ChangeEvent } from "react";
import { BASE_URL } from "../config";
import { Validations } from "../services/Validations";
import "./AddNewCarPage.css";
import car1 from "../images/car1.jpg";
import car2 from "../images/car2.jpg";
import car3 from "../images/car3.jpg";
import { ImageSlider } from "./ImageSlider";

interface AddNewCarPageProps {
  repository: CarRepository;
}

const AddNewCarPage = (props: AddNewCarPageProps) => {
  const navigate = useNavigate();
  const IMAGES = [car1, car2, car3];
  const [imgUrls, setImgUrls] = useState<string[]>([]);
  const [id, setId] = useState<number | null>(parseId(useParams().id));
  const [selectedFile, setSelectedFile] = useState<File | undefined>();
  const [details, setDetails] = useState({
    brand: "",
    age: 1998,
    description: "",
    price: 1000,
  });

  async function saveCar(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (!Validations.validateBrand(details.brand)) {
      alert("Nespravny format znacky");
      return;
    }
    if (!Validations.validateDescription(details.description)) {
      alert("Nespravny format opisu auta");
      return;
    }
    if (!Validations.validateAge(details.age)) {
      alert("Nespravny format roku");
      return;
    }
    const newCar: CarEditDto = {
      id: 0,
      brand: details.brand,
      age: details.age,
      description: details.description,
      price: details.price,
    };

    console.log(newCar);

    if (id == null) {
      const id = await props.repository.add(newCar);
      setId(id);
      console.log(id);
    } else {
      await props.repository.edit(id, newCar);
    }
    await savePhoto();
  }

  const handleChange = (e: FormEvent<HTMLInputElement>) => {
    const { name, value } = e.currentTarget;
    setDetails({ ...details, [name]: value });
  };

  async function fillData() {
    if (id == null) {
      return;
    }
    const car = await props.repository.getById(id);

    const carImgUrls = car.imageIds.map(
      (imgId) => `${BASE_URL}/cars/${id}/photo/${imgId}?time=${Date.now()}`
    );

    setImgUrls(carImgUrls);
    console.log(carImgUrls);

    setDetails({
      brand: car.brand,
      age: car.age,
      description: car.description,
      price: car.price,
    });
    console.log(car);
  }

  const handleTextAreaChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    const { name, value } = e.currentTarget;
    setDetails({ ...details, [name]: value });
  };

  async function updateSelectedFile(e: ChangeEvent<HTMLInputElement>) {
    if (e.target.files == null) {
      return;
    }

    setSelectedFile(e.target.files[0]);
  }

  async function savePhoto() {
    if (selectedFile == null || id == null) {
      return;
    }

    props.repository.savePhoto(id, selectedFile);
    console.log(selectedFile);
    navigate("/");
  }

  useEffect(() => {
    fillData();
  }, []);

  const imgUrl = id ? `${BASE_URL}/cars/${id}/photo?time=${Date.now()}` : null;
  const mainTitle = id ? "Uprav auto" : "Pridaj nove auto";
  return (
    <section>
      <form onSubmit={saveCar}>
        <h1>{mainTitle}</h1>
        <h3>Značka :</h3>{" "}
        <input
          type="text"
          value={details.brand}
          name="brand"
          onChange={handleChange}
          minLength={2}
        />
        <h3>Rok výroby :</h3>{" "}
        <input
          type="number"
          value={details.age}
          name="age"
          onChange={handleChange}
          min={1980}
          max={2024}
        />
        <h3>Opis auta:</h3>{" "}
        <textarea
          name="description"
          className="area"
          onChange={handleTextAreaChange}
          value={details.description}
          minLength={15}
        />
        <h3>Cena auta: (v eurách)</h3>
        <input
          type="range"
          name="price"
          step={10}
          value={details.price}
          onChange={handleChange}
          min={0}
          max={100000}
        />
        {details.price} €
        <br />
        <h3>Fotka: </h3>
        <div className="slider">
        <ImageSlider imageUrls={imgUrls } />
        </div>
     
        {/* {imgUrl ? <img src={imgUrl} /> : null} */}
        <br />
        <div className="add-photo-section">
          <input
            type="file"
            name="imgUrl"
            className="add-photo-button"
            onChange={updateSelectedFile}
          />
          <button className="add-newcar-button" type="submit">
            Potvrď
          </button>
        </div>
      </form>
    </section>
  );
};

function parseId(param: string | undefined) {
  const id = Number(param);
  return Number.isNaN(id) ? null : id;
}

export default AddNewCarPage;
