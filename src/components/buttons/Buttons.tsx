import "./Buttons.css"
export interface ButtonProps {
  onClick: () => void;
}
interface BaseButtonProps {
  title: string;
  onClick: () => void;
}

export const DeleteAllCarsButton = (props: ButtonProps) => {
   return (
    <BaseButton
      title=" Vymazať všetky autá"
      onClick={props.onClick}
    />
  );
};

export const DeleteCarButton = (props: ButtonProps) => {
  return (
    <BaseButton
      title="Vymaž auto"
      onClick={props.onClick}
    />
  );
};

export const ReloadAllCarsButton = (props: ButtonProps) => {
   return (
    <BaseButton
      title="Refreshni autá"
      onClick={props.onClick}
    />
  );
};

export const EditCarButton = (props: ButtonProps) => {
  return (
    <BaseButton
      title="Uprav auto"
      onClick={props.onClick}
    />
  );
};

export const AddNewCarButton = (props: ButtonProps) => {
  return (
    <BaseButton
      title="Pridaj auto"
      onClick={props.onClick}
    />
  );
};

const BaseButton = (props: BaseButtonProps) => {
  return (
    <button  onClick={props.onClick}>
      {props.title}
    </button>
  );
};
