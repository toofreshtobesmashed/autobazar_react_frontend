export type CarEditDto = {
  id: number;
  brand: string;
  age: number;
  description: string;
  price: number;
  // img: string;
};

export type CarDto = {
  id: number;
  brand: string;
  age: number;
  description: string;
  price: number;
  imageIds: number[];
};

export type CarListDto = {
  cars: CarDto[];
};
