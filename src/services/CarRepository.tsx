import { Car } from "../components/Types";
import { CarListDto, CarEditDto,CarDto } from "./CarTypes";

export class CarRepository {
  private readonly baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  public async getAll(): Promise<CarListDto> {
    const res = await fetch(this.baseUrl);
    const data = await res.json();
    return data;
  }

  public async deleteById(id: number) {
    await fetch(`${this.baseUrl}/${id}`, { method: "DELETE" });
  }

  public async deleteAll() {
    await fetch(this.baseUrl, { method: "DELETE" });
  }

  public async add(car: CarEditDto): Promise<number> {
    const res = await fetch(`${this.baseUrl}`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(car),
    });
    const data = await res.json();
    return data.id;
  }

  public async edit(id: number, car: CarEditDto) {
    await fetch(`${this.baseUrl}/${id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(car),
    });
  }

  public async getById(id: number): Promise<CarDto> {
    const res = await fetch(`${this.baseUrl}/${id}`);
    const data = await res.json();
    return data;
  }

  public async savePhoto(id: number, selectedFile: File) {
    const formData = new FormData();
    formData.append("file", selectedFile);
    await fetch(`${this.baseUrl}/${id}/photo`, {
      method: "POST",
      body: formData,
    });
  }
  
}
