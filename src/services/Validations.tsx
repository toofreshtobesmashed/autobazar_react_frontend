export class Validations {
  private static brandPattern = /[a-zA-Z ]{2,32}/;
  private static descriptionPattern = /[a-zA-Z ]{2,100}/;

  public static validateBrand(brand: string) {
    return this.brandPattern.test(brand);
  }

  public static validateDescription(description: string) {
    return this.descriptionPattern.test(description);
  }

  public static validateAge(age: number) {
    return 1980 <= age && age <= new Date().getFullYear();
  }
}
